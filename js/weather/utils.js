    // Oculta el spinner y muestra las tarjetas de clima
    function hideSpinner() {
        let spinner = document.getElementById('spinner');
        spinner.style.visibility = "hidden";
    }

    // Oculta todas las tarjetas de clima hasta que se complete el API request
    function hideWeatherCards() {
        document.getElementById('weatherCards').style.visibility = "hidden";
    }

    function showWeatherCards() {
        document.getElementById('weatherCards').style.visibility = "visible";
    }