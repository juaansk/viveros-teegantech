// Elementos del DOM : Clima
let spinner = document.getElementById('spinner');

let location_indicator = document.getElementById('location_indicator');
let dateTime_indicator = document.getElementById('dateTime_indicator');
const today = new Date(Date.now());
const dateOptions = {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric'
};
let status_indicator = document.getElementById('status_indicator');
let temp_indicator = document.getElementById('temp_indicator');
let humidity_indicator = document.getElementById('humidity_indicator');
let wind_indicator = document.getElementById('wind_indicator');

// Elementos del DOM : Alertas

// array con todas las alertas de la api
let alertsByDayArr = [];

let accordion1 = document.getElementById('day1');
let accordion2 = document.getElementById('day2');
let accordion3 = document.getElementById('day3');
let accordion4 = document.getElementById('day4');
let accordion1Alerts = '';
let accordion2Alerts = '';
let accordion3Alerts = '';
let accordion4Alerts = '';



const weatherURL = 'https://weatherservices.herokuapp.com/api/weather/';
const alertsURL = 'https://weatherservices.herokuapp.com/api/alerts/byDay/';

async function fetchWeather() {

    const res = await fetch(weatherURL);
    const resJson = await res.json();
    return resJson;
};

async function fetchAlerts(day) {
    const res = await fetch(alertsURL + '' + day);
    const resJson = await res.json();
    return resJson.alerts;
};

async function loadAlerts() {
    for (let i = 0; i < 4; i++) {
        let alerts = await fetchAlerts(i);
        alertsByDayArr.push(alerts);
    }
}

/*
async function showAlertsByDay(day){
  console.log('respuesta de alertas para el dia ', day);
  let alerts = await getAlertsByDay(day);
  console.log('las alertillas son:' , alerts);

  // Agrego contenido de las alertas en los accordions
  for(let i = 0; i < alerts; i++){
    
  }
};
 
 Esta funcion hara el request de alertas para el dia recibido una unica vez y solo en caso de que aun no exista en memoria. 
 * Devuelve un array de alertas para el dia recibido.
 

async function getAlertsByDay(day){
  try{
  switch(day){
    case 0:
      if(accordion1Alerts == '') {
        accordion1Alerts = await fetchAlerts(day);
      }
      document.getElementById('alert1-title').innerText = accordion1Alerts[0].status + ' ' + accordion1Alerts[0].title;
      

      return accordion1Alerts;
    case 1:
      if(accordion2Alerts == ''){
        accordion2Alerts = await fetchAlerts(day);
      }
      return accordion2Alerts;
    case 2:
      if(accordion3Alerts == '') {
        accordion3Alerts = await fetchAlerts(day);
      }
      return accordion3Alerts;
    case 3:
      if(accordion4Alerts == '') {
        accordion4Alerts = await fetchAlerts(day);
      }
      return accordion4Alerts;
    }
  }
  catch(err){
    console.log('Se produjo un error al obtener las alertas por dia.', err);
  }
}
*/

// funcion inicial
(async function() {
    console.log('Se estan cargando las API requests...')
    hideWeatherCards();

    // CLIMA
    try {
        const weatherData = await fetchWeather();
        hideSpinner();
        showWeatherCards();
        console.log('Clima data: ', weatherData);

        let forecastData = weatherData.items[0].forecast.forecast;
        // array de datos del pronostico de los proximos dias(0,1,2,3)
        let forecastDataByDay = Object.values(forecastData);
        // Elimino el primer objeto que no se a que corresponde
        forecastDataByDay.shift();
        console.log(forecastDataByDay);


        // Actualizar info principal de weather cards
        location_indicator.innerText = weatherData.items[0].name + ', ' + weatherData.items[0].province;
        dateTime_indicator.innerText = today.toLocaleDateString('es-ES', dateOptions) + ', ';
        status_indicator.innerText = weatherData.items[0].weather.description;
        temp_indicator.innerText = weatherData.items[0].weather.temp + '°';
        humidity_indicator.innerText = 'Precipitación: ' + weatherData.items[0].weather.humidity + '% ';
        wind_indicator.innerText = 'Vientos: ' + weatherData.items[0].weather.wind_speed + ' km/h ';
    } catch (err) {
        console.error("Se ha producido un error al consultar a la API de Weather");
        hideSpinner();
    }

    // ALERTAS
    try {
        await loadAlerts();
        console.log('alerts por dia', alertsByDayArr);
        document.getElementById('alert1-dateTime').innerText = alertsByDayArr[0][0].date + ' ' + alertsByDayArr[0][0].hour;
        document.getElementById('alert1-title').innerText = alertsByDayArr[0][0].status + ' ' + alertsByDayArr[0][0].title;
        document.getElementById('alert1-description').innerText = alertsByDayArr[0][0].description;

        document.getElementById('alert2-dateTime').innerText = alertsByDayArr[0][1].date + ' ' + alertsByDayArr[0][1].hour;
        document.getElementById('alert2-title').innerText = alertsByDayArr[0][1].status + ' ' + alertsByDayArr[0][1].title;
        document.getElementById('alert2-description').innerText = alertsByDayArr[0][1].description;

        document.getElementById('alert3-dateTime').innerText = alertsByDayArr[0][2].date + ' ' + alertsByDayArr[0][2].hour;
        document.getElementById('alert3-title').innerText = alertsByDayArr[0][2].status + ' ' + alertsByDayArr[0][2].title;
        document.getElementById('alert3-description').innerText = alertsByDayArr[0][2].description;

        document.getElementById('alert3-dateTime').innerText = alertsByDayArr[0][3].date + ' ' + alertsByDayArr[0][3].hour;
        document.getElementById('alert3-title').innerText = alertsByDayArr[0][3].status + ' ' + alertsByDayArr[0][3].title;
        document.getElementById('alert3-description').innerText = alertsByDayArr[0][3].description;


    } catch (err) {
        console.log("se ha producido un error al consultar a la API de Alertas", err)
    }


})();