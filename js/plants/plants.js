function getFamiliesNames() {
    let familiesNames = [];
    for (const family of families) {
        familiesNames.push(family.name);
    }
    return familiesNames;
}

function getFamilyDataByName(familyName) {
    let familyFound = {};
    for (const family of families) {
        if (family.name == familyName)
            familyFound = family;
    }
    return familyFound;
}

// Elementos DOM del Formulario
let selectFamilies = document.getElementById('families-select');
let familyBox = document.getElementById('plant-family');
let originBox = document.getElementById('plant-origin');
let leafTypeBox = document.getElementById('plant-leafType');
let fruitTypeBox = document.getElementById('plant-fruitType');
let seedTypeBox = document.getElementById('plant-seedType');


function loadSelectFamilyValues() {
    console.log('Cargando la lista de familias...');
    //let families = getFamiliesNames();
    for (let i = 0, familiesArrLength = families.length; i < familiesArrLength; i++) {
        let familyItem = families[i];
        let option = new Option(familyItem.name, familyItem.name, true);
        selectFamilies.options.add(option);
    }
    selectFamilies.addEventListener("change", function() {
        console.log("Se selecciono la opcion: ", selectFamilies.value);
        autoCompleteFamilyBoxes(selectFamilies.value);
    })
    console.log('Familias cargadas!')
}

function autoCompleteFamilyBoxes(selectedFamily) {
    console.log(selectedFamily)
    let familyData = getFamilyDataByName(selectedFamily);

    document.querySelector("#plant-family + label").textContent = familyData.name;
    document.querySelector("#plant-origin + label").textContent = familyData.origin;
    document.querySelector("#plant-leafType + label").textContent = familyData.leafType;
    document.querySelector("#plant-fruitType + label").textContent = familyData.fruitType;
    document.querySelector("#plant-seedType + label").textContent = familyData.seedType;

    disableFamilyBoxes();
}

function enableFamilyDataInput() {

    restoreFamilyBoxes();
    enableFamilyBoxes();
}

function restoreFamilyBoxes() {
    document.querySelector("#plant-family + label").textContent = "Familia";
    document.querySelector("#plant-origin + label").textContent = "Origen";
    document.querySelector("#plant-leafType + label").textContent = "Tipo de hoja";
    document.querySelector("#plant-fruitType + label").textContent = "Tipo de fruto";
    document.querySelector("#plant-seedType + label").textContent = "Tipo de semilla";
}

function enableFamilyBoxes() {
    familyBox.disabled = false;
    originBox.disabled = false;
    leafTypeBox.disabled = false;
    fruitTypeBox.disabled = false;
    seedTypeBox.disabled = false;
}

function disableFamilyBoxes() {
    familyBox.disabled = true;
    originBox.disabled = true;
    leafTypeBox.disabled = true;
    fruitTypeBox.disabled = true;
    seedTypeBox.disabled = true;
}

function init() {
    loadSelectFamilyValues();
}

init();