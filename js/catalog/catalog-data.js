catalogo = [

    {
        id: 1,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "rojo",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 2,
        familia: "ACERACEAE",
        nombre: "Berro",
        nombreCientifico: "Roripa nasturtium-acquaticum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 3,
        familia: "ACTINIDIACEAE",
        nombre: "totorilla",
        nombreCientifico: "Cyperus odoratus",
        clase: 'clase 1',
        color: "amarillo",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 4,
        familia: "AGAVACEAE",
        nombre: "cebollin",
        nombreCientifico: "Cyperus rotundus",
        clase: 'clase 1',
        color: "blanco",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 5,
        familia: "AGAVACEAE",
        nombre: "campanilla",
        nombreCientifico: "Ipomoea cairica",
        clase: 'clase 2',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 6,
        familia: "ACERACEAE",
        nombre: "repollito de agua",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 7,
        familia: "ACANTHACEAE",
        nombre: "totora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "amarillo",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 8,
        familia: "AIZOACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "rojo",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 9,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 10,
        familia: "AIZOACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 11,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 12,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 13,
        familia: "ACTINIDIACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 14,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "amarillo",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 15,
        familia: "ACANTHACEAE",
        nombre: "hierba mora",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 16,
        familia: "ACTINIDIACEAE",
        nombre: "heliotropo",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "morado",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    },
    {
        id: 17,
        familia: "ACANTHACEAE",
        nombre: "eclipta",
        nombreCientifico: "Solanum sublobatum",
        clase: 'clase 1',
        color: "verde",
        fruto: "tomate",
        suelo: "neutro",
        cicloRiego: "",
        temperatura: 30
    }



]